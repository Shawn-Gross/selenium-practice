package org.selenium.practice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.selenium.practice.framework.BasePage;

/**
 * Example Page that represents the Bing Search Engine Page (http://bing.com)
 * Created by Shawn Gross on 1/31/2018.
 */
public class ExamplePage extends BasePage{

    public ExamplePage(WebDriver driver)
    {
        super(driver);
    }
    /**
     * Locators go here
     */
    private static final By searchInput  = By.xpath(".//input[@id='sb_form_q']");
    private static final By searchButton = By.xpath(".//input[@id='sb_form_go']");


    public boolean clickSearchInput()
    {
        waitForElement(searchInput, webDriver);
        webDriver.findElement(searchInput).click();
        return true;
    }

    public boolean searchTextAndValidate(String searchPhrase)
    {
        waitForElement(searchInput, webDriver);
        WebElement searchInputElement = webDriver.findElement(searchInput);
        searchInputElement.sendKeys(searchPhrase);
        String value = searchInputElement.getAttribute("value");
        if(value.equalsIgnoreCase(searchPhrase))
        {
            return true;
        }
        return false;
    }

    public boolean clicksearchButton()
    {
        waitForElement(searchButton, webDriver);
        webDriver.findElement(searchButton).click();
        return true;
    }
}
