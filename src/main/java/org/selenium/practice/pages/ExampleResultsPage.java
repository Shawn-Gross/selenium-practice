package org.selenium.practice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.selenium.practice.framework.BasePage;

/**
 * Simpel page to show that we landed on the correct page after the search. This can be expanded to test for results etc.
 * Created by Shawn Gross on 2/2/2018.
 */
public class ExampleResultsPage extends BasePage {

    public ExampleResultsPage(WebDriver driver)
    {
        super(driver);
    }

    private static final By linkTitledAllInResultsPage = By.xpath(".//a[text()='All']");

    public boolean validateLandedOnResultsPage()
    {
        waitForElement(linkTitledAllInResultsPage, webDriver);
        webDriver.findElement(linkTitledAllInResultsPage);
        return true;
    }
}
