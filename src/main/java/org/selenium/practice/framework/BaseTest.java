package org.selenium.practice.framework;

import org.openqa.selenium.WebDriver;

/**
 * The base test is a super class to each test. It has the logic to set the driver executables(what runs the drivers inthe browser)
 * and determine which driver to use
 * Created by Shawn Gross on 2/2/2018.
 */
public class BaseTest {

    /**
     * static block. this is always executed first when this class is instantiated and is pretty rare
     */
    {
        setDriverExecutables();
    }

    /**
     * This takes in a string (from test level) and then gives your web driver at the test level, the proper driver for thwateever web browser you want
     * @param browser
     * @return
     */
    public static WebDriver getDriver(String browser)
    {
        DriverType driverType = new DriverType();
        switch(browser.toLowerCase())
        {
            case "firefox":
                driverType.useFireFoxDriver();
                return driverType.getDriver();
            case "chrome":
                driverType.useChromeDriver();
                return driverType.getDriver();
            default:
                driverType.useFireFoxDriver();
                return driverType.getDriver();
        }
    }

    /**
     * Sets executables you need these in order to run selenium on each browser. firefox and chrome are here
     */
    public void setDriverExecutables()
    {
        System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
    }
}
