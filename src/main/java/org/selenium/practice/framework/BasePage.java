package org.selenium.practice.framework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Shawn Gross on 2/2/2018.
 */
public class BasePage {
    public WebDriver webDriver;
    private int explicitWaitTime;

    public BasePage(WebDriver driver)
    {
        webDriver = driver;
        explicitWaitTime = 30;
    }

    public int getExplicitWaitTime()
    {
        return explicitWaitTime;
    }

    public void setExplicitWaitTime(int explicitWaitTime)
    {
        this.explicitWaitTime = explicitWaitTime;
    }

    public void waitForElement(By locator, WebDriver driver)
    {
        waitForElementWithDefaultExplicitWaitTime(locator, driver, getExplicitWaitTime());
    }

    public void waitForElementWithDefaultExplicitWaitTime(By locator, WebDriver driver, int timeOutValue)
    {
        WebDriverWait wait = new WebDriverWait(driver, timeOutValue);
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }




}
