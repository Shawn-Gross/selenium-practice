package org.selenium.practice.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Class used to set the appropriate driver for what browser you are using
 * Created by Shawn Gross on 2/2/2018.
 */
public class DriverType {
    public WebDriver driver;

    public DriverType()
    {
        super();
    }

    public void useFireFoxDriver()
    {
        this.driver = new FirefoxDriver();
    }

    public void useChromeDriver()
    {
        this.driver = new ChromeDriver();
    }

    public WebDriver getDriver()
    {
        return driver;
    }
}
