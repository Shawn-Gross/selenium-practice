package org.selenium.practice.tests;

import org.openqa.selenium.WebDriver;
import org.selenium.practice.framework.BaseTest;
import org.selenium.practice.pages.ExamplePage;
import org.selenium.practice.pages.ExampleResultsPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;

/**
 * Created by Shawn Gross on 1/31/2018.
 */
public class ExampleTest extends BaseTest
{

    WebDriver driver;

    @BeforeClass
    public void before()
    {
        //declared browser type
        driver = getDriver("firefox");
    }

    @Test
    public void test() throws InterruptedException {

        driver.get("https://www.bing.com/");
        ExamplePage ep = new ExamplePage(driver);
        Assert.assertTrue(ep.clickSearchInput());
        // Hard coded waits( Thread.sleep()) shouldnt be used normally. I just put these here so you can see the test
        Thread.sleep(5000);
        Assert.assertTrue(ep.searchTextAndValidate("shotguns"));
        Thread.sleep(5000);
        Assert.assertTrue(ep.clicksearchButton());
        Thread.sleep(5000);

        ExampleResultsPage erp = new ExampleResultsPage(driver);
        Assert.assertTrue(erp.validateLandedOnResultsPage());
    }


    @AfterClass
    public void after()
    {
        //close out browser
        driver.quit();
    }


}
